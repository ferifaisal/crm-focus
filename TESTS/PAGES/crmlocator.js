const x = function (selector){
    return {
    selector : selector,
    locateStrategy: 'xpath'
    }
};

const timeOut = 60000;

const crmlocator = {
    urlSAMA : 'https://devproxy.astra.co.id/hsohome/#/login',
    urlCRM : 'https://mission2wdevelopment.azurewebsites.net/PartialHome',
    elements : {
       cekverify : '.astra-logo',
       username : '#username',
       password : '#password',
       KlikLogin : '.btn-login',
       KlikMenuCRM : '.row:nth-child(7) > .col-lg-3:nth-child(1) .info-box-text',
       KlikMenuScenario : '.fa-pencil-square-o',
       KlikMenuScenarioList : '.text-center:nth-child(3) > .sub a',
       KlikBuatBaru : '.padding-left',
       NamaScenario : '#nama-scenario',
       PeriodeMulai : '#datepicker',
       PeriodeSelesai : '#datepicker2',
       KlikSelanjutnya : '.col-xs-12 > .col-xs-6 > #nextstep',
       KlikTambahCustomer : '#tambahcustomer',
       DaftarTargetCustomer : x('//li[1]//label[@class="k-radio-label"]'),
       NamaTargetCustomer : '#input-name',
       ddlProfilPekerjaan : x('//div[2]/div/div/div[2]/div/div'),
       ProfilPekerjaan : x('//li[contains(.,"TNI/Polri")]'),
       lblProfilPekerjaan : x('//label[contains(.,"Pekerjaan")]'),
       ddlJenisKelamin : x('//span/span[2]/span'),
       JenisKelamin : x('//li[contains(.,"Laki-laki")]'),
       ddlAgama : x('//div[3]/div[2]/span/span/span[2]/span'),
       valueAgama : x('//li[contains(.,"Islam")]'),
       ddlCustomerDari : x('//div[9]/div[2]/span/span/span'),
       selectValue : '#ca839a52-3072-4a5d-84b5-f81109b4f8af',
       valueCustomerDari : x('/html[1]/body[1]/div[19]/div[1]/div[3]/ul[1]/li[1]'),
       ddlTujuan : x('//div[10]/div[2]/span/span/span[2]/span'),
       valueTujuan : x('/html[1]/body[1]/div[7]/div[1]/div[3]/ul[1]/li[1]'),
       KlikCheckLeads : x('/html[1]/body[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/div[2]/div[1]/div[2]/fieldset[1]/div[1]/div[5]/div[1]/div[3]/button[1]'),
       KlikNext : x('/html[1]/body[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/div[2]/div[1]/div[2]/fieldset[1]/div[1]/div[6]/button[2]'),
       CheckboxTelp : x('//ul[@id="panelbar2"]//label[@class="k-checkbox-label k-item k-state-default"]'),
       KlikSimpan : x('//input[@class="k-button bttn k-primary padding-right padding-left"]'),                    
       KlikLogout : 'li:nth-child(4) span'
    },
    commands : [{
        navigateSAMA(){
            this
            .navigate(crmlocator.urlSAMA)
            .waitForElementVisible(crmlocator.elements.cekverify)
        },
        navigateCRM(){
            this
            .navigate(crmlocator.urlCRM)
            .waitForElementVisible(crmlocator.elements.KlikMenuScenario)
        },
        signInAT(){
            this
            .waitForElementVisible(crmlocator.elements.username)
            .click(crmlocator.elements.username)
            .waitForElementVisible(crmlocator.elements.KlikLogin)
            .setValue(crmlocator.elements.username, 'aridwi022657')
            .setValue(crmlocator.elements.password, 'Astra@123')
            .waitForElementVisible(crmlocator.elements.KlikLogin)
            .click(crmlocator.elements.KlikLogin)
            .waitForElementVisible(crmlocator.elements.KlikMenuCRM)
            .click(crmlocator.elements.KlikMenuCRM)
        } ,
        createScenario(){
            this
            //Masuk Ke New Tab CRM Focus
            .waitForElementVisible(crmlocator.elements.KlikMenuScenario)
            .click(crmlocator.elements.KlikMenuScenario)
            .waitForElementVisible(crmlocator.elements.KlikMenuScenarioList)
            .click(crmlocator.elements.KlikMenuScenarioList)
            .waitForElementVisible(crmlocator.elements.KlikBuatBaru, timeOut)
            .click(crmlocator.elements.KlikBuatBaru)
            .waitForElementVisible(crmlocator.elements.NamaScenario)
            .setValue(crmlocator.elements.NamaScenario, 'Test CRM Focus')
            .setValue(crmlocator.elements.PeriodeMulai, '22 October 2019')
            .setValue(crmlocator.elements.PeriodeSelesai, '29 October 2019')
            .waitForElementVisible(crmlocator.elements.KlikSelanjutnya)
            .click(crmlocator.elements.KlikSelanjutnya)
            .waitForElementVisible(crmlocator.elements.DaftarTargetCustomer, timeOut)
            .waitForElementVisible(crmlocator.elements.KlikTambahCustomer)
            .click(crmlocator.elements.KlikTambahCustomer)
            .waitForElementVisible(crmlocator.elements.NamaTargetCustomer)
            .setValue(crmlocator.elements.NamaTargetCustomer, 'Luhut Panjaitan')
            .waitForElementVisible(crmlocator.elements.ddlProfilPekerjaan)
            .click(crmlocator.elements.ddlProfilPekerjaan)
            .waitForElementVisible(crmlocator.elements.ProfilPekerjaan)
            .click(crmlocator.elements.ProfilPekerjaan)
            .click(crmlocator.elements.lblProfilPekerjaan)
            .waitForElementVisible(crmlocator.elements.ddlJenisKelamin)
            .click(crmlocator.elements.ddlJenisKelamin)
            .waitForElementVisible(crmlocator.elements.JenisKelamin, timeOut)
            .click(crmlocator.elements.JenisKelamin, timeOut)
            .waitForElementVisible(crmlocator.elements.ddlAgama, timeOut)
            .click(crmlocator.elements.ddlAgama, timeOut)
            .waitForElementVisible(crmlocator.elements.valueAgama, timeOut)
            .click(crmlocator.elements.valueAgama, timeOut)
            .waitForElementVisible(crmlocator.elements.ddlCustomerDari, timeOut)
            .click(crmlocator.elements.ddlCustomerDari, timeOut)
            .waitForElementVisible(crmlocator.elements.valueCustomerDari, timeOut)
            .click(crmlocator.elements.valueCustomerDari, timeOut)
            .waitForElementVisible(crmlocator.elements.ddlTujuan, timeOut)
            .click(crmlocator.elements.ddlTujuan, timeOut)
            .waitForElementVisible(crmlocator.elements.valueTujuan)
            .click(crmlocator.elements.valueTujuan, timeOut)
            //.waitForElementVisible(crmlocator.elements.KlikCheckLeads, timeOut)
            .click(crmlocator.elements.KlikCheckLeads, timeOut)
            .waitForElementVisible(crmlocator.elements.KlikNext, timeOut)
            .click(crmlocator.elements.KlikNext, timeOut)
            //.waitForElementVisible(crmlocator.elements.CheckboxTelp, timeOut)
            //.click(crmlocator.elements.CheckboxTelp, timeOut)
            //.waitForElementVisible(crmlocator.elements.KlikSimpan, timeOut)
            .click(crmlocator.elements.KlikSimpan, timeOut)
            .pause(600000)
        } ,
        logOut(){
            this
            .waitForElementVisible(crmlocator.elements.KlikLogout)
            .pause(2000)
            .click(crmlocator.elements.KlikLogout)
       }
    }]
}

module.exports = crmlocator;