// const seleniumServerPath = require('selenium-server').path;
// const chromedriverPath = require('chromedriver').path;

// module.exports = {
//     src_folders: './tests/steps',
//     output_folder: './reports',
//     page_objects_path: './tests/pages',
//     custom_assertions_path: '',
//     live_output: false,
//     disable_colors: false,
//     webdriver: {
//         default_path_prefix: '/wd/hub'
//     },
//     selenium: {
//         start_process: true,
//         server_path: seleniumServerPath,
//         log_path: './log',
//         cli_args: {
//             'webdriver.chrome.driver': "E:/AUTOMATION/chromedriver.exe",
//         },
//         selenium_host: '127.0.0.1',
//         selenium_port: 4444,
//     },
//     test_settings: {
//         default: {
//             desiredCapabilities: {
//               browserName: "chrome",
//               javascriptEnabled: true,
//               chromeOptions: {
//                   w3c: false,
//                   args: [
//                         "disable-web-security",
//                         "ignore-certificate-errors",
//                         "--test-type",
//                         "start-maximized",
//                         // "--headless"
//                   ]
//               }
//             }
//         },
//     },
// };


module.exports = {
    globals_path : './globals.js', 
    src_folders: './tests/steps',
    output_folder: './reports',
    page_objects_path: './tests/pages',
    custom_assertions_path: '',
    live_output: false,
    disable_colors: false,
    default_path_prefix: '',
    selenium_host : 'localhost',
    selenium_port : 9515,
    test_settings: {
        default: {
            desiredCapabilities: {
                browserName: "chrome",
                javascriptEnabled: true,
                chromeOptions: {
                    // w3c: false, 
                    args: [
                        "disable-web-security",
                        "ignore-certificate-errors",
                        "--test-type",
                        "start-maximized",
                        // "--headless"
                    ]
                }
            }
        },
    },
};